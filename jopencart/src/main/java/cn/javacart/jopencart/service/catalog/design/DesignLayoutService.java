package cn.javacart.jopencart.service.catalog.design;

import java.util.List;

import cn.javacart.jopencart.model.LayoutModule;

import com.jfinal.aop.Duang;
import com.jfinal.plugin.activerecord.Db;

/**
 * 设计布局service
 * @author farmer
 *
 */
public class DesignLayoutService {
	
	public final static DesignLayoutService ME = Duang.duang(DesignLayoutService.class);
	
	/**
	 * 获取布局
	 * @param route
	 * @return
	 */
	public Integer getLayout(String route){
		return Db.queryInt("SELECT layout_id FROM joc_layout_route WHERE ? LIKE route AND store_id = ? ORDER BY route DESC LIMIT 1", route,0);
	}
	
	/**
	 * 获取布局位置相关的组件
	 * @param layoutId
	 * @param position
	 * @return
	 */
	public List<LayoutModule> getLayoutModules(Integer layoutId, String position){
		return LayoutModule.ME.find("select * from joc_layout_module t where t.layout_id = ? and t.position = ? ORDER BY sort_order",layoutId,position);
	}
	
}
