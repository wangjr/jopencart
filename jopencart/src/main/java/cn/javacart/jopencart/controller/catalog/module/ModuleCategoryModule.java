package cn.javacart.jopencart.controller.catalog.module;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;
import cn.javacart.jopencart.kit.SetKit;
import cn.javacart.jopencart.model.Category;
import cn.javacart.jopencart.service.catalog.catalog.CatalogCategoryService;
import cn.javacart.jopencart.service.catalog.catalog.CatalogProductService;
import cn.javacart.jopencart.util.ChainMap;

import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 幻灯片
 * @author farmer
 *
 */
public class ModuleCategoryModule extends JOpencartModule{
	
	public ModuleCategoryModule(JOpencartController controller) {
		super(controller);
	}
	
	public ModuleCategoryModule(JOpencartController controller,Object extra) {
		super(controller);
	}
	
	
	@Override
	public String render() {
		language.load("/catalog/language/{0}/module/category.php");
		dataMap.put("heading_title", language.get("heading_title"));
		String[] parts = new String[2];
		if(SetKit.isset(getPara("path"))){
			parts = getPara("path").split("_");
		};
		
		if (SetKit.isset(parts[0])) {
			dataMap.put("category_id", MemoryUtils.valueOf(parts[0]));
		} else {
			dataMap.put("category_id",  MemoryUtils.valueOf(0));
		}
		if (parts.length > 1 && SetKit.isset(parts[1])) {
			dataMap.put("child_id",  MemoryUtils.valueOf(parts[1]));
		} else {
			dataMap.put("child_id",  MemoryUtils.valueOf(0));
		}
		
		
		List<Map<String, Object>> categories = new ArrayList<Map<String,Object>>();
		for (Category category : CatalogCategoryService.ME.getCategories(0)) {
			
			List<Map<String, Object>> childrenData = new ArrayList<Map<String,Object>>();
			if(category.getInt("category_id") == dataMap.get("category_id").toInteger()){
				for (Category child : CatalogCategoryService.ME.getCategories(category.getInt("category_id"))) {
					
					childrenData.add(
							ChainMap.createMap()
							.put("category_id", child.get("category_id"))
							.put("name", child.getStr("name") + 
									(SetKit.isset(config.getInt("config_product_count")) ?  "(" + 
									CatalogProductService.ME.getTotalProducts(
											ChainMap.createMap()
											.put("filter_category_id", child.getInt("category_id"))
											.put("filter_sub_category", true).toMap())
									+")" : ""))
							.put("href", url.linkStr("product/category", "path="+category.getInt("category_id")+"_"+child.getInt("category_id")))
							.toMap());
				}
			}
			
			categories.add(
					ChainMap.createMap()
					.put("category_id", category.get("category_id"))
					.put("children", childrenData)
					.put("name", category.getStr("name") + 
									(SetKit.isset(config.getInt("config_product_count")) ?  "(" + 
									CatalogProductService.ME.getTotalProducts(
											ChainMap.createMap()
											.put("filter_category_id", category.getInt("category_id"))
											.put("filter_sub_category", true).toMap())
									+")" : ""))
					.put("href", url.linkStr("product/category", "path="+category.getInt("category_id")))
					.toMap());
		}
		dataMap.put("categories", MemoryUtils.valueOf(categories));	//分类
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/module/category.tpl", dataMap));
	}

}
