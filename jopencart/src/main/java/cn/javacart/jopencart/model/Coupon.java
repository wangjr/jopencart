/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_coupon表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|coupon_id           |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(128)        |false |false   |NULL    |
|code                |VARCHAR(10)         |false |false   |NULL    |
|type                |CHAR(1)             |false |false   |NULL    |
|discount            |DECIMAL(15)         |false |false   |NULL    |
|logged              |BIT(0)              |false |false   |NULL    |
|shipping            |BIT(0)              |false |false   |NULL    |
|total               |DECIMAL(15)         |false |false   |NULL    |
|date_start          |DATE(10)            |false |false   |0000-00-00|
|date_end            |DATE(10)            |false |false   |0000-00-00|
|uses_total          |INT(10)             |false |false   |NULL    |
|uses_customer       |VARCHAR(11)         |false |false   |NULL    |
|status              |BIT(0)              |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Coupon extends Model<Coupon>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2365241483282236524L;
	/**
	 * 用于查询操作
	 */
	public static final Coupon ME = new Coupon();
	
}