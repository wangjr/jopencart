/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_order_voucher表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|order_voucher_id    |INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|voucher_id          |INT(10)             |false |false   |NULL    |
|description         |VARCHAR(255)        |false |false   |NULL    |
|code                |VARCHAR(10)         |false |false   |NULL    |
|from_name           |VARCHAR(64)         |false |false   |NULL    |
|from_email          |VARCHAR(96)         |false |false   |NULL    |
|to_name             |VARCHAR(64)         |false |false   |NULL    |
|to_email            |VARCHAR(96)         |false |false   |NULL    |
|voucher_theme_id    |INT(10)             |false |false   |NULL    |
|message             |TEXT(65535)         |false |false   |NULL    |
|amount              |DECIMAL(15)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class OrderVoucher extends Model<OrderVoucher>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2442471483282244247L;
	/**
	 * 用于查询操作
	 */
	public static final OrderVoucher ME = new OrderVoucher();
	
}