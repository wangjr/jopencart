/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_product_reward表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|product_reward_id   |INT(10)             |false |true    |NULL    |
|product_id          |INT(10)             |false |false   |0|
|customer_group_id   |INT(10)             |false |false   |0|
|points              |INT(10)             |false |false   |0|
+--------------------+--------------------+------+--------+--------+--------

 */
public class ProductReward extends Model<ProductReward>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2456871483282245687L;
	/**
	 * 用于查询操作
	 */
	public static final ProductReward ME = new ProductReward();
	
}