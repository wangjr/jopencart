/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_product_description表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|product_id          |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(255)        |false |false   |NULL    |
|description         |TEXT(65535)         |false |false   |NULL    |
|tag                 |TEXT(65535)         |false |false   |NULL    |
|meta_title          |VARCHAR(255)        |false |false   |NULL    |
|meta_description    |VARCHAR(255)        |false |false   |NULL    |
|meta_keyword        |VARCHAR(255)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class ProductDescription extends Model<ProductDescription>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2446441483282244644L;
	/**
	 * 用于查询操作
	 */
	public static final ProductDescription ME = new ProductDescription();
	
}