/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_tax_class表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|tax_class_id        |INT(10)             |false |true    |NULL    |
|title               |VARCHAR(32)         |false |false   |NULL    |
|description         |VARCHAR(255)        |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class TaxClass extends Model<TaxClass>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2479141483282247914L;
	/**
	 * 用于查询操作
	 */
	public static final TaxClass ME = new TaxClass();
	
}