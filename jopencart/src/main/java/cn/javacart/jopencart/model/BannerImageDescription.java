/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_banner_image_description表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|banner_image_id     |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |NULL    |
|banner_id           |INT(10)             |false |false   |NULL    |
|title               |VARCHAR(64)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class BannerImageDescription extends Model<BannerImageDescription>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2351771483282235177L;
	/**
	 * 用于查询操作
	 */
	public static final BannerImageDescription ME = new BannerImageDescription();
	
}