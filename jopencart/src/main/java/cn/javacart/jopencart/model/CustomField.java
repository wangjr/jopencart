/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_custom_field表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|custom_field_id     |INT(10)             |false |true    |NULL    |
|type                |VARCHAR(32)         |false |false   |NULL    |
|value               |TEXT(65535)         |false |false   |NULL    |
|validation          |VARCHAR(255)        |false |false   |NULL    |
|location            |VARCHAR(7)          |false |false   |NULL    |
|status              |BIT(0)              |false |false   |NULL    |
|sort_order          |INT(10)             |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class CustomField extends Model<CustomField>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2371831483282237183L;
	/**
	 * 用于查询操作
	 */
	public static final CustomField ME = new CustomField();
	
}