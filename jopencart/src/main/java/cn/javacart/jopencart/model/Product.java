/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_product表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|product_id          |INT(10)             |false |true    |NULL    |
|model               |VARCHAR(64)         |false |false   |NULL    |
|sku                 |VARCHAR(64)         |false |false   |NULL    |
|upc                 |VARCHAR(12)         |false |false   |NULL    |
|ean                 |VARCHAR(14)         |false |false   |NULL    |
|jan                 |VARCHAR(13)         |false |false   |NULL    |
|isbn                |VARCHAR(17)         |false |false   |NULL    |
|mpn                 |VARCHAR(64)         |false |false   |NULL    |
|location            |VARCHAR(128)        |false |false   |NULL    |
|quantity            |INT(10)             |false |false   |0|
|stock_status_id     |INT(10)             |false |false   |NULL    |
|image               |VARCHAR(255)        |true  |false   |NULL    |
|manufacturer_id     |INT(10)             |false |false   |NULL    |
|shipping            |BIT(0)              |false |false   |1|
|price               |DECIMAL(15)         |false |false   |0.0000|
|points              |INT(10)             |false |false   |0|
|tax_class_id        |INT(10)             |false |false   |NULL    |
|date_available      |DATE(10)            |false |false   |0000-00-00|
|weight              |DECIMAL(15)         |false |false   |0.00000000|
|weight_class_id     |INT(10)             |false |false   |0|
|length              |DECIMAL(15)         |false |false   |0.00000000|
|width               |DECIMAL(15)         |false |false   |0.00000000|
|height              |DECIMAL(15)         |false |false   |0.00000000|
|length_class_id     |INT(10)             |false |false   |0|
|subtract            |BIT(0)              |false |false   |1|
|minimum             |INT(10)             |false |false   |1|
|sort_order          |INT(10)             |false |false   |0|
|status              |BIT(0)              |false |false   |0|
|viewed              |INT(10)             |false |false   |0|
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Product extends Model<Product>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2443841483282244384L;
	/**
	 * 用于查询操作
	 */
	public static final Product ME = new Product();
	
}