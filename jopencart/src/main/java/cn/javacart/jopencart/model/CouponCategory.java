/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_coupon_category表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|coupon_id           |INT(10)             |false |true    |NULL    |
|category_id         |INT(10)             |false |true    |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class CouponCategory extends Model<CouponCategory>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2366541483282236654L;
	/**
	 * 用于查询操作
	 */
	public static final CouponCategory ME = new CouponCategory();
	
}